function main() {
    console.log('Hello world!');
    console.log('And again!');

    const array = [5, 1, 3, 2, 4];
    const sortedArray = sort(array);
    console.log("Unsorted array: ", array);
    console.log("Sorted array: ", sortedArray);
}

function sort(array: number[]): number[] {
    const sortedArray = [...array];
    for (let i = 0; i < sortedArray.length - 1; i++) {
        for (let j = i + 1; j < sortedArray.length; j++) {
            if (sortedArray[i] > sortedArray[j]) {
                const temp = sortedArray[i];
                sortedArray[i] = sortedArray[j];
                sortedArray[j] = temp;
            }
        }
    }
    return sortedArray;
}

try {
    main();
} catch (e) {
    console.error(e);
}